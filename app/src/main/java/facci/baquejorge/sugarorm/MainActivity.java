package facci.baquejorge.sugarorm;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button ButtonGuardar, ButtonModificar,
    ButtonEliminar, ButtonConsultaIndividual;
    EditText txtTitulo, txtEdicion, txtID ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButtonGuardar = (Button) findViewById(R.id.ButtonGuardar);
        ButtonModificar = (Button) findViewById(R.id.ButtonModificar);
        ButtonEliminar = (Button) findViewById(R.id.ButtonEliminar);
        ButtonConsultaIndividual = (Button) findViewById(R.id.ButtonConsultaIndividual);

        txtTitulo = (EditText) findViewById(R.id.edit_titulo);
        txtEdicion = (EditText) findViewById(R.id.edit_edicion);
        txtID = (EditText)findViewById(R.id.EditTextID);

        ButtonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book registro1 = new Book(
                        txtTitulo.getText().toString(),
                        txtEdicion.getText().toString());
                registro1.save();
                Log.e("Guardar","Datos Guardados");
            }
        });

        ButtonConsultaIndividual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Book book = Book.findById(
                        Book.class,
                        Long.parseLong(txtID.getText().toString()
                ));
                txtTitulo.setText(book.getTitle());
                txtEdicion.setText(book.getEdition());
            }
        });

        ButtonModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Book book = Book.findById(Book.class, Long.parseLong(
                        txtID.getText().toString()));
                book.title = txtTitulo.getText().toString() ;
                book.edition = txtEdicion.getText().toString();
                book.save();
            }
        });

        ButtonEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Book book = Book.findById(Book.class, Long.parseLong
                        (txtID.getText().toString()));
                book.title = txtTitulo.getText().toString() ;
                book.edition = txtEdicion.getText().toString();
                book.delete();
            }
        });
    }
}
